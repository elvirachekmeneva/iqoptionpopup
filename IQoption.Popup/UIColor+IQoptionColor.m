//
//  UIColor+IQoptionColor.m
//  IQoption.Popup
//
//  Created by Эльвира on 24.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import "UIColor+IQoptionColor.h"

@implementation UIColor (IQoptionColor)

+ (instancetype) iqOption_blackBgrColor
{
    return rgbColor(19.0,  24.0,  36.0);
}

+ (instancetype) iqOption_activeItemColor
{
    return rgbaColor(255.0, 255.0, 255.0, 0.02);
}

+ (instancetype) iqOption_inactiveItemColor
{
    return rgbaColor(255.0, 255.0, 255.0, 0.1);
}

+ (instancetype) iqOption_orangeColor
{
    return rgbaColor(249.0, 110.0, 50.0, 1.0);
}

@end
