//
//  PopoverViewController.m
//  IQoption.Popup
//
//  Created by Эльвира on 24.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import "PopoverViewController.h"
#import "UIColor+IQoptionColor.h"
#import "IntervalItemCollectionViewCell.h"

typedef NS_ENUM(NSUInteger, IQselectedBtnFirstGroup) {
    IQselectedBtnFirstGroupFirst,
    IQselectedBtnFirstGroupSecond,
    IQselectedBtnFirstGroupThird,
    IQselectedBtnFirstGroupFourth
};

typedef NS_ENUM(NSUInteger, IQselectedBtnSecondGroup) {
    IQselectedBtnSecondGroupFirst,
    IQselectedBtnSecondGroupSecond
};

static const CGFloat kBackgroundCornerRadius = 5.0;
static const CGFloat kButtonsCornerRadius = 3.0;
static const CGFloat kButtonsDefaultHeight = 32.0;
static const CGFloat kButtonsDefaultSpace = 12.0;
static const CGFloat kAnimationDuration = 0.2;
static const CGFloat KCollectionViewItemWidth = 30;
static NSString* const  kCollectionViewCellID = @"IntervalItemCollectionViewCell";

@interface PopoverViewController () <UIPopoverPresentationControllerDelegate>
{
    IQselectedBtnFirstGroup  selectedItemFirstGroup;
    IQselectedBtnSecondGroup selectedItemSecondGroup;
    NSArray *activeIconsNames;
    NSArray *inactiveIconsNames;
    NSArray *intervalNames;
    BOOL subBtnsVisible;
    
}


@end

@implementation PopoverViewController 

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.modalPresentationStyle = UIModalPresentationPopover;
        self.popoverPresentationController.delegate = self;
    }
    
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.backgroundColor = [UIColor iqOption_blackBgrColor];
    activeIconsNames = @[@"area_active.png", @"line_active.png", @"candles_active.png", @"bars_active.png"];
    inactiveIconsNames = @[@"area.png", @"line.png", @"candles.png", @"bars.png"];
    intervalNames = @[@"15s",@"30s", @"1m", @"2m", @"5m", @"10m", @"15m", @"30m", @"1h", @"2h", @"4h", @"8h", @"12h", @"1d", @"1w", @"1M"];

    UINib *cvCellNib = [UINib nibWithNibName:kCollectionViewCellID bundle: nil];
    [self.intervalCollectionView registerNib:cvCellNib forCellWithReuseIdentifier:kCollectionViewCellID];
    [self.selectedIntervalCollectionView registerNib:cvCellNib forCellWithReuseIdentifier:kCollectionViewCellID];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [self setupAllSubviews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



# pragma mark - Popover delegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone; //You have to specify this particular value in order to make it work on iPhone.
}

- (void)prepareForPopoverPresentation:(UIPopoverPresentationController *)popoverPresentationController {
    self.popoverPresentationController.sourceView = self.sourceView ? self.sourceView : self.view;
    self.popoverPresentationController.sourceRect = self.sourceRect;
    self.preferredContentSize = self.contentSize;
    
    popoverPresentationController.permittedArrowDirections = self.arrowDirection ? self.arrowDirection : UIPopoverArrowDirectionAny;
    popoverPresentationController.passthroughViews = self.passthroughViews;
    popoverPresentationController.backgroundColor = self.backgroundColor;
    popoverPresentationController.popoverLayoutMargins = self.popoverLayoutMargins;
}


# pragma mark UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return intervalNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IntervalItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellID forIndexPath:indexPath];
    cell.title.text = intervalNames[indexPath.row];
//    cell.backgroundColor = [UIColor purpleColor];
    if ([collectionView isEqual:self.selectedIntervalCollectionView]) {
        cell.title.textColor = [UIColor iqOption_orangeColor];
        cell.title.alpha = 1.0;
//        cell.backgroundColor = [UIColor grayColor];
    }
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:self.intervalCollectionView]) {
        [self move:collectionView toIndex:indexPath decelerate:NO];
        
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.intervalCollectionView]) {
        self.selectedIntervalCollectionView.contentOffset = self.intervalCollectionView.contentOffset;
    }
    
    if ([scrollView isEqual:self.selectedIntervalCollectionView]) {
        self.intervalCollectionView.contentOffset = self.selectedIntervalCollectionView.contentOffset;
    }
    
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
  
    [self move:scrollView toIndex:[self selectedItemIndex] decelerate:decelerate];
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [self move:scrollView toIndex:[self selectedItemIndex]  decelerate:NO];
    
}

- (NSIndexPath*) selectedItemIndex
{
    CGPoint selectedViewCenter = self.selectedIntervalView.center;
    
    CGPoint pointAtIntervalCV = [self.view convertPoint:selectedViewCenter toView:self.intervalCollectionView];
    NSIndexPath *index = [self.intervalCollectionView indexPathForItemAtPoint:pointAtIntervalCV];
    
    NSLog(@"pointAtIntervalCV = %f", pointAtIntervalCV.x);
    NSLog(@"index = %ld", (long)index.item);
    return index;
    
}
- (void) move:(UIScrollView *) scrollView toIndex:(NSIndexPath*)index decelerate:(BOOL)decelerate
{
    
    CGFloat leftItemsWidth = self.view.bounds.size.width/2 - self.selectedIntervalView.frame.size.width/2;
    NSInteger count1_2 = leftItemsWidth / KCollectionViewItemWidth;
    CGFloat littlePeace = leftItemsWidth - count1_2*KCollectionViewItemWidth;
    
    CGFloat leftIntervalInset = 0;
    if (count1_2  >= index.item) {
        
        leftIntervalInset = (count1_2 - index.item)*KCollectionViewItemWidth + littlePeace;
        
        [self.intervalCollectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        self.intervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, leftIntervalInset, 0.0, 0.0);
        self.selectedIntervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, leftIntervalInset, 0.0, 0.0);
        
                return;
    }
    CGFloat rightInset = 0;
    NSInteger diff = intervalNames.count - index.item;
    if ((count1_2 + 1) >= diff) {
       
        
        rightInset = ((count1_2 + 1) - diff) *KCollectionViewItemWidth + littlePeace;
        
        NSLog(@"right = %f", rightInset);
        [self.intervalCollectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
        self.intervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, rightInset);
        self.selectedIntervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, rightInset);
        
                return;
    }
    if (!decelerate) {
        self.intervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.selectedIntervalCollectionView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        [self.intervalCollectionView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }

    
}


# pragma mark VC setup

- (void) setupAllSubviews{
    _backgroundView.backgroundColor = [UIColor iqOption_blackBgrColor];
    _backgroundView.layer.cornerRadius = kBackgroundCornerRadius;
    _selectedIntervalView.layer.cornerRadius = kButtonsCornerRadius;
    _selectedIntervalView.layer.borderColor = [UIColor iqOption_orangeColor].CGColor;
    _selectedIntervalView.layer.borderWidth = 1.0;
    _selectedIntervalView.backgroundColor = [UIColor clearColor];
    subBtnsVisible = YES;
    
    [self roundedButtonsInView:_mainBtnsContainerView];
    [self roundedButtonsInView:_subBtnsContainerView];
    
    [self selectBtnInFirstGroupWithType:IQselectedBtnFirstGroupFirst];
    [self hideSubButtonsAnimated:NO];
    [self autoScalingActivate:NO];
    [self selectBtnInSecondGroupWithType:IQselectedBtnSecondGroupFirst];
    
    
    
    
}

- (void) roundedButtonsInView:(UIView*) view{
    UIBezierPath *maskPAth1 = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(kButtonsCornerRadius, kButtonsCornerRadius) ];
    CAShapeLayer *maskLayer1 = [CAShapeLayer new];
    maskLayer1.frame = view.bounds;
    maskLayer1.path = maskPAth1.CGPath;
    view.layer.mask = maskLayer1;
    
}

# pragma mark IBAction methods

- (IBAction)button1Selected:(id)sender {
    [self selectBtnInFirstGroupWithType:IQselectedBtnFirstGroupFirst];
    [self hideSubButtonsAnimated:YES];
    [self autoScalingActivate:NO];
    
}

- (IBAction)button2Selected:(id)sender {
    [self selectBtnInFirstGroupWithType:IQselectedBtnFirstGroupSecond];
    [self hideSubButtonsAnimated: YES];
    [self autoScalingActivate:NO];
}

- (IBAction)button3Selected:(id)sender {
    [self selectBtnInFirstGroupWithType:IQselectedBtnFirstGroupThird];
    [self showSubButtons];
    [self autoScalingActivate:YES];

    
    
}

- (IBAction)button4Selected:(id)sender {
    [self selectBtnInFirstGroupWithType:IQselectedBtnFirstGroupFourth];
    [self hideSubButtonsAnimated:YES];
    [self autoScalingActivate:NO];
    
}
- (IBAction)button5Selected:(id)sender {
    [self selectBtnInSecondGroupWithType:IQselectedBtnSecondGroupFirst];
}
- (IBAction)button6Selected:(id)sender {
    [self selectBtnInSecondGroupWithType:IQselectedBtnSecondGroupSecond];
}

# pragma mark Actions help methods

- (void) hideSubButtonsAnimated:(BOOL) animated
{
    if (subBtnsVisible) {
        
        
        
        [self.view layoutIfNeeded];
        
        if (!animated) {
            [self hideSubButtons];
            return;
        }
        
         [UIView animateWithDuration:kAnimationDuration delay:0.0 options:UIViewAnimationOptionLayoutSubviews  animations:^{
             
             [self hideSubButtons];
             
         } completion:^(BOOL finished) {
             
         }];
      
    }
    
}

- (void) hideSubButtons
{
    _subBtnsHeightConstraint.constant     = 0 ;
    _subBtsBottomSpaceConstraint.constant = 0 ;
    self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, self.contentSize.height - kButtonsDefaultHeight - kButtonsDefaultSpace);
    [self.view layoutIfNeeded];
    subBtnsVisible = NO;
}

- (void) autoScalingActivate:(BOOL) activate
{
    _scalingLabel.enabled = activate;
    _scalingSwitcher.enabled = activate;
}

- (void) showSubButtons
{
    if (!subBtnsVisible) {
        
        
        [self.view layoutIfNeeded];
        
        
        [UIView animateWithDuration:kAnimationDuration delay:0.0 options:UIViewAnimationOptionLayoutSubviews  animations:^{
            
            [self updateConstraintsForSubButtons];
            subBtnsVisible = YES;
            self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height + kButtonsDefaultHeight + kButtonsDefaultSpace);
            [self.view layoutIfNeeded]; // Called on parent view
            self.subBtnsContainerView.alpha = 1.0;
        } completion:^(BOOL finished) {
            
        }];
    }
    
    
}

- (void) updateConstraintsForSubButtons
{
    _subBtnsHeightConstraint.constant     = subBtnsVisible ? 0 : kButtonsDefaultHeight;
    _subBtsBottomSpaceConstraint.constant = subBtnsVisible ? 0 : kButtonsDefaultSpace;
}

- (void) selectBtnInFirstGroupWithType:(IQselectedBtnFirstGroup)type
{
    [self buttonActivationForState:YES withType:type fromStackView:_mainBtnsStackView changeImage:YES];
    
    
    if (type != selectedItemFirstGroup) {
        
        [self buttonActivationForState:NO withType:selectedItemFirstGroup fromStackView:_mainBtnsStackView changeImage:YES];
        
        
    }
    selectedItemFirstGroup = type;
    
    
}
- (void) selectBtnInSecondGroupWithType:(IQselectedBtnSecondGroup)type
{
    [self buttonActivationForState:YES withType:type fromStackView:_subBtnsStackView changeImage:NO];
    
    if (type != selectedItemSecondGroup) {
        
        [self buttonActivationForState:NO withType:selectedItemSecondGroup fromStackView:_subBtnsStackView changeImage:NO];
        
    }
    selectedItemSecondGroup = type;
    
}


- (void) buttonActivationForState:(BOOL) state withType:(NSUInteger)type fromStackView:(UIStackView*) stackView changeImage:(BOOL)changeImage
{
    UIButton *button = (UIButton*) stackView.arrangedSubviews[type];
    button.selected = state;
    
    
    [button setBackgroundColor:state ? [UIColor iqOption_activeItemColor] : [UIColor iqOption_inactiveItemColor]];
    
    if (changeImage) {
        [self changeImageForType:type toActive:state forButton:button];
    }
}

- (void) changeImageForType:(NSUInteger)type toActive:(BOOL)active forButton:(UIButton*)button
{
    if (active) {
        UIImage *image = [UIImage imageNamed:activeIconsNames[type]];
        [button setImage:image forState:UIControlStateNormal|UIControlStateSelected];
    }
    else {
        UIImage *image = [UIImage imageNamed:inactiveIconsNames[type]];
        [button setImage:image forState:UIControlStateNormal|UIControlStateSelected];
    }
}



@end
