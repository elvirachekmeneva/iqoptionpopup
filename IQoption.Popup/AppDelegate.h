//
//  AppDelegate.h
//  IQoption.Popup
//
//  Created by Эльвира on 23.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

