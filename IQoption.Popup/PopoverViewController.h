//
//  PopoverViewController.h
//  IQoption.Popup
//
//  Created by Эльвира on 24.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PopoverDelegate <NSObject>

@optional

/*!
 Popover notifies the delegate, that the popover needs to reposition it's location.
 */
- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view;

/*!
 Popover asks the delegate, whether it should dismiss itself.
 */
- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController;

/*!
 Popover notifies the delegate, that popover did dismiss itself.
 */
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController;

@end


@interface PopoverViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *intervalCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *scalingLabel;
@property (weak, nonatomic) IBOutlet UISwitch *scalingSwitcher;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *mainBtnsContainerView;
@property (weak, nonatomic) IBOutlet UIView *subBtnsContainerView;


//имена кнопок условные, т.к. я не знаю как они правильно называются)
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *button5;
@property (weak, nonatomic) IBOutlet UIButton *button6;

@property (weak, nonatomic) IBOutlet UIStackView *mainBtnsStackView;
@property (weak, nonatomic) IBOutlet UIStackView *subBtnsStackView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subBtnsHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subBtsBottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIView *selectedIntervalView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectedIntervalCollectionView;





@property (nonatomic, weak) id<PopoverDelegate> delegate;

@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, weak) UIView *sourceView;
@property (nonatomic, assign) UIPopoverArrowDirection arrowDirection;
@property (nonatomic, assign) CGRect sourceRect;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) NSArray *passthroughViews;
@property (nonatomic, assign) UIEdgeInsets popoverLayoutMargins;


@end
