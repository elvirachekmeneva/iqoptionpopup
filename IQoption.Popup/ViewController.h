//
//  ViewController.h
//  IQoption.Popup
//
//  Created by Эльвира on 23.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopoverViewController.h"
@interface ViewController : UIViewController <PopoverDelegate>

@property (weak, nonatomic) IBOutlet UIButton *popUpBtn;

@end

