//
//  UIColor+IQoptionColor.h
//  IQoption.Popup
//
//  Created by Эльвира on 24.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>

#define rgbaColor(r, g, b, a) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:a]
#define rgbColor(r, g, b) rgbaColor(r, g, b, 1.0)

@interface UIColor (IQoptionColor)

+ (instancetype) iqOption_blackBgrColor;

+ (instancetype) iqOption_activeItemColor;

+ (instancetype) iqOption_inactiveItemColor;

+ (instancetype) iqOption_orangeColor;

@end
