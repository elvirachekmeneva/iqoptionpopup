//
//  ViewController.m
//  IQoption.Popup
//
//  Created by Эльвира on 23.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import "ViewController.h"


static const CGFloat kPopoverDefaultWidth = 260.0;
static const CGFloat kPopoverDefaultHeight = 204.0;

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)presentPopup:(id)sender {
    NSString *popoverVcId = [NSString stringWithFormat:@"%@",[PopoverViewController class]];
    PopoverViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:popoverVcId];
    
    
    vc.sourceView = self.popUpBtn; //The view containing the anchor rectangle for the popover.
    vc.arrowDirection = UIPopoverArrowDirectionDown;
    vc.sourceRect = CGRectMake(CGRectGetMidX(_popUpBtn.bounds), _popUpBtn.bounds.origin.y, 0, 0); //The rectangle in the specified view in which to anchor the popover.
    vc.contentSize = CGSizeMake(kPopoverDefaultWidth, kPopoverDefaultHeight);
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];

}


@end
