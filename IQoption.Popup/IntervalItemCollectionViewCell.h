//
//  IntervalItemCollectionViewCell.h
//  IQoption.Popup
//
//  Created by Эльвира on 26.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntervalItemCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
