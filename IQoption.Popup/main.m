//
//  main.m
//  IQoption.Popup
//
//  Created by Эльвира on 23.01.17.
//  Copyright © 2017 i. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
